#!/bin/bash

#Directory where the service expects the certificates
CRT_OUTPUT="/etc/certs/"

#Services to reload
SERVICES="haproxy znc"

#Privkey file
PRIVKEY="privkey.pem"

#Full Chain file
FULLCHAIN="fullchain.pem"

#Certificate Extension
CRT_EXT=".pem"

for DOMAIN in $RENEWED_DOMAINS; do
        cat "$RENEWED_LINEAGE/$PRIVKEY" "$RENEWED_LINEAGE/$FULLCHAIN" > "$CRT_OUTPUT$DOMAIN$CRT_EXT"
done

for SERVICE in $SERVICES; do
	service $SERVICE reload;
done


